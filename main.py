#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import sys
sys.path.append('model')
sys.path.append('base')

from base.lstm import deeplog, loganomaly, robustlog
from model.logdeep.tools.predict import Predicter
from model.logdeep.tools.train import Trainer
from model.logdeep.tools.utils import *


algo_type = "log_anomaly"
if algo_type == "log_anomaly":
    from conf.config_loganomaly import options
else:
    from conf.config_loganomaly import options

seed_everything(seed=1234)


def train():
    Model = loganomaly(input_size=options['input_size'],
                       hidden_size=options['hidden_size'],
                       num_layers=options['num_layers'],
                       num_keys=options['num_classes'])
    trainer = Trainer(Model, options)
    trainer.start_train()


def predict():
    Model = loganomaly(input_size=options['input_size'],
                       hidden_size=options['hidden_size'],
                       num_layers=options['num_layers'],
                       num_keys=options['num_classes'])
    predicter = Predicter(Model, options)
    # predicter.predict_unsupervised()
    predicter.predict_single_one()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('mode', choices=['train', 'predict'])
    args = parser.parse_args()
    if args.mode == 'train':
        train()
    else:
        predict()
