DATA_DIR="../data/open_stack"
DATA_PARSER_DIR=$DATA_DIR/log_parser

#mkdir $DATA_PARSER_DIR
#python pre_process.py $DATA_DIR/openstack_abnormal.log $DATA_PARSER_DIR/openstack_abnormal_content.log
#python pre_process.py $DATA_DIR/openstack_normal1.log $DATA_PARSER_DIR/openstack_normal1_content.log
#python pre_process.py $DATA_DIR/openstack_normal2.log $DATA_PARSER_DIR/openstack_normal2_content.log
#
#cat $DATA_PARSER_DIR/openstack_normal* > $DATA_PARSER_DIR/all_normal.log
#
#python ft_tree/main_train.py \
#  -train_log_path $DATA_PARSER_DIR/openstack_normal2_content.log \
#  -out_seq_path $DATA_PARSER_DIR/out.train.temp.seq \
#  -templates $DATA_PARSER_DIR/out.train.temp \
#  -short_threshold 3 -FIRST_COL 0
#
#python concat.py $DATA_PARSER_DIR/out.train.temp.seq $DATA_PARSER_DIR/out.train.seq

#python ft_tree/main_match.py \
#  -logs $DATA_PARSER_DIR/openstack_normal1_content.log \
#  -template $DATA_PARSER_DIR/out.train.temp | python deal_match_results.py > $DATA_PARSER_DIR/out.valid.seq

python ft_tree/main_match.py \
  -logs $DATA_PARSER_DIR/openstack_abnormal_content.log \
  -template $DATA_PARSER_DIR/out.train.temp | python deal_match_results.py > $DATA_PARSER_DIR/out.test.seq
