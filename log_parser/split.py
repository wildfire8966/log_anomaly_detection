import sys


def split(file_name, line_num, percent):
    limit_num = int(line_num * percent)
    items = file_name.split('/')
    data_dir = '/'.join(items[:-1])
    cnt = 0
    with open(file_name, 'r') as f, open(data_dir + '/train.logs', 'w+') as f1, open(data_dir + '/valid.logs', 'w+') as f2:
        for line in f:
            if cnt < limit_num:
                f1.write(line + '\n')
            else:
                f2.write(line + '\n')


if __name__ == "__main__":
    split(sys.argv[1], 0.7)
