#!/usr/bin/python
# coding=utf-8

import sys


def process_open_stack(input_file_path, output_file_path):
    cnt = 0
    right_cnt = 0
    with open(input_file_path, 'r') as f, open(output_file_path, 'w+') as f1:
        for line in f:
            cnt += 1
            line = line.strip()
            items = line.split(' ')
            right_block_ix = line.find("]")
            content = line[right_block_ix + 1:]
            content = content.strip()
            if content:
                content = content.replace('[', '')
                content = content.replace(']', '')
                f1.write(content + '\n')
                right_cnt += 1
        print("total records nums: %s, right records nums: %s." % (cnt, right_cnt))


if __name__ == "__main__":
    arg1 = sys.argv[1]
    arg2 = sys.argv[2]
    process_open_stack(arg1, arg2)
    # process_open_stack("../data/open_stack/openstack_abnormal.log", "../data/open_stack/openstack_abnormal_content.log")
    # process_open_stack("../data/open_stack/openstack_normal1.log", "../data/open_stack/openstack_normal1_content.log")
    # process_open_stack("../data/open_stack/openstack_normal2.log", "../data/open_stack/openstack_normal2_content.log")