import sys

def concat_to_one_line(input_file, output_file):
    rs = []
    with open(input_file, 'r') as f, open(output_file, 'w+') as f1:
        for line in f:
            line = line.strip()
            if line.startswith("-"):
                continue
            rs.append(line)
        f1.write(" ".join(rs) + "\n")


if __name__ == "__main__":
    concat_to_one_line(sys.argv[1], sys.argv[2])