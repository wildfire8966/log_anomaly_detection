import sys


for line in sys.stdin:
    try:
        line = line.strip()
        if not line:
            continue
        if line.startswith("---"):
            continue
        items = line.split(' ')
        t = eval(' '.join(items[1:]))
        event_id = t[0]
        event_val = t[1]

        print(event_id, end=' ')
    except:
        print(0, end=' ')